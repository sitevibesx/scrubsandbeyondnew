    /*
        Gather data here that might change depending on a users interaction with
        the page.  For example, changing a color option might change a products
        image.  Or changing a size might affect the products price.  Basically,
        if you can envision something possibly being dynamic gather it here
    */
SiteVibes.gatherDynamicInfo = function(field){
    var ref = this;
    this.q.trendingType = field;

    for(k in this.configurableDataPoints){
        var foundVariation = false;
        for(i=0;i<this.configurableDataPoints[k].length;i++){
            //should only report errors on failing the last variation
            if(!foundVariation){
                this.q[(k == 'sale_price' ? 'price' : k)] = this.safeGrab({errorReporting:(i == (this.configurableDataPoints[k].length - 1) ? true : false),
                           selector:this.configurableDataPoints[k][i],
                           customParse: function(el){
                                                el = el[0];
                                                if(k != 'image'){
                                                    //just grab the textcontent of text nodes
                                                    if(el.textContent){
                                                        //the sale price logic assumes sale price will always execute after price
                                                        foundVariation = true;
                                                        return (k == 'sale_price' ? ref.q.price+',' :'')+el.textContent.trim();
                                                    }
                                                }else{
                                                    //if its an image
                                                    if(el.src){
                                                        foundVariation = true;
                                                        return el.src;
                                                    }
                                                    //as a last resort try getting the background image of the element
                                                    var style = el.currentStyle || window.getComputedStyle(el, false);
                                                    var bg = style.backgroundImage;
                                                    if(bg != '' && bg != 'none'){
                                                        foundVariation = true;
                                                        bg = bg.replace(/url([^h]*/, '');
                                                        bg = bg.replace(/['"]*)/,'');
                                                        return bg;
                                                    }
                                                }
                                            }
                          });
                
            }
        }
    }
    
};

/*
    This data gathering function is only fired on initial page load!  Its only 
    purpose really is to be a little bit more efficient in gathering data from 
    the page.  Gather stuff here that WILL NOT change, like the url of the page,
    title of the page, etc.  Anything that can change like an option or price
    should not be gathered here!
*/
SiteVibes.gatherStaticInfo = function(session, fdate){
    this.q.session         = ((session)?encodeURIComponent(session):"");

                                
    this.q.id              = encodeURIComponent(this.tmiID);
    this.q.productCategory = (this.productCategory ? this.productCategory:"");
    this.q.pagetype        = (this.role ? encodeURIComponent(this.role) : "");
    this.q.url             = encodeURIComponent(location.href);
    this.q.host            = encodeURIComponent(location.host);
    this.q.title           = encodeURIComponent(document.title);
    this.q.time            = fdate;

};

/*
    Determine which elements on the page trigger a sitevibes reporting action 
    when clicked.  IE, facebook like buttons, tweet buttons, add to cart buttons,
    etc.
*/
SiteVibes.setSocialListeners = function(){
    var ref = this;
    window.onclick = function (e) {
    
        var svEvent = e.target.id;
        
        if (svEvent === null || svEvent === '') {
            svEvent = e.target.className;
        }
        

       /* if (svEvent == 'at4-icon aticon-pinterest') {
            ref.checkIntentType('pinterest');
        }
        
        if (svEvent == 'at4-icon aticon-twitter') {
            ref.checkIntentType('twitter');
           
        }

        if (svEvent == 'at4-icon aticon-facebook') {
            ref.checkIntentType('facebook');
        }*/

        if(svEvent == 'atg_behavior_addItemToCart') {
            ref.checkIntentType('addToCart');
            ref.analytics.actions.addToCart = (
                ref.analytics.actions.addToCart ? ref.analytics.actions.addToCart++ 
                : 1
            );
        }
    }
};


/*
some final custom validation to make sure we should actually share
you can check that product options are actually set and stuff here

or make sure any other required action or element exists or doesnt


return false to cancel reporting
return true to go ahead with reporting (default)

*/
SiteVibes.verifyOkToReport = function(){

    var options = document.querySelectorAll('.rcbInput');

    //if its a pageViews event don't validate options
    if(this.q.trendingType != 'pageViews'){
        for(ii=0;ii<options.length;ii++){
            //if an option isn't set, return and dont do the share
            if(options[ii].id != 'ctl00_seesearch23_radcbSearch_HawkInput' &&
                options[ii].value == 'Choose..'){
                return false;
            }
        }
    }

    return true;
};

SiteVibes.addSocialShareButtons = function(){
    return;  //only after we get permission
    var shareHTML = '<div id="sv_share_wrap"><div id="sv_share">';
    for(i=0;i<this.socialShareButtons.length;i++){
        /*
        only add it if it has a share handler on the sitevibes object.
        strict naming convention is:
        '__SHARE_NAME__'+'Share', 
        so 'facebook' would be 'facebookShare
        */

        if(typeof(this[this.socialShareButtons[i]+'Share']) == 'function'){
            shareHTML += '<div onclick="SiteVibes.'+this.socialShareButtons[i]+'Share();" \
                          class="sv_share sv_'+this.socialShareButtons[i]+'"></div>';
        }
    }
    shareHTML += '</div></div>';

    document.querySelector(this.injectSocialButtonsAfter).insertAdjacentHTML('afterend', shareHTML);
};

SiteVibes.facebookShare = function(){
    var text  = (this.q.name && this.q.name != '' ? this.q.name : '');
    var win = window.open('https://facebook.com/sharer.php?u='+encodeURIComponent(document.location.href)+'&t='+text, 'targetWindow', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,height=120');

};

SiteVibes.twitterShare = function(){
    var text  = (this.q.name && this.q.name != '' ? this.q.name : '');
    var via   = (this.clientTwitterName && this.clientTwitterName != '' ? this.clientTwitterName : '');
    var win = window.open('https://twitter.com/intent/tweet?url='+encodeURIComponent(document.location.href)+'&text='+text+'&via='+via, 'targetWindow', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,height=120');
};

SiteVibes.pinterestShare = function(){
    var media = (this.q.image && this.q.image != '' ? this.q.image : '');
    var text  = (this.q.name && this.q.name != '' ? this.q.name : '');

    var win = window.open('https://www.pinterest.com/pin/create/button/?url='+encodeURIComponent(document.location.href)+'&media='+media+'&description='+text, 'targetWindow', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,height=120');
}
